QUnit.test("hello_test_2", function(assert) {
    assert.ok(1 == "1", "Passed");
});

QUnit.test("fail_test_2", function(assert) {
    assert.ok(2 == "2", "Failed");
});

QUnit.test("async_test", function(assert) {
    stop(); // pause test
    setTimeout(function() {
        assert.ok(true);
        start(); // start test
    }, 1000);
});
