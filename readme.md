Unit testing JavaScript via PyGtk
=================================

The general idea is to use QUnit (http://qunitjs.com/) from within
a PyGtk application with a webview component.
When the tests have run, inspect the HTML to see the result and
exit the application with a proper exit code.

You can get a list of executed tests with the -v option

TODO:
* allow xml output (xUnit compatible)

the tests can then be invoked via:

testrunner.py <path to test.js file> <path to 2nd test.js file> ...
