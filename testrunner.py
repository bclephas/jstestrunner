#!/usr/bin/env python

#http://webkitgtk.org/reference/webkitgtk/stable/ch02.html
#https://docs.python.org/2.6/library/optparse.html?highlight=optparse#module-optparse
#http://www.pygtk.org/pygtk2reference/class-gtkwindow.html#constructor-gtkwindow
#http://blog.motane.lu/2009/06/18/pywebkitgtk-execute-javascript-from-python/
#http://webkitgtk.org/reference/webkitgtk/stable/webkitgtk-webkitwebview.html

import os
import sys
import gtk
import webkit
from optparse import OptionParser

import gobject
from HTMLParser import HTMLParser

exit_status = 0
global_webview = None
verbose = False

def get_html_body(webview, frame):
    webview.execute_script('oldtitle=document.title;document.title=document.documentElement.innerHTML;')
    doc = frame.get_title()
    webview.execute_script('document.title=oldtitle;')
    return doc
    
"""
Parse a structure like:
    <ol id="qunit-tests">
        <li class="pass" id="qunit-test-output1"><strong><span class="test-name">hello_test</span></strong></li>
        <li class="fail" id="qunit-test-output2"><strong><span class="test-name">fail_test</span></strong></li>
    </ol>
"""
class ParseTestResults(HTMLParser):
    tc_results = {}
    tc_result = 'unknown'
    read_tc_name = False

    def has_attr(self, attrs, attr):
        try:
            return [item[0] for item in attrs].index(attr)
        except ValueError:
            return -1

    def handle_starttag(self, tag, attrs):
        if tag == 'li':
            class_idx = self.has_attr(attrs, 'class')
            id_idx = self.has_attr(attrs, 'id')

            if class_idx >= 0 and id_idx >= 0:
                self.tc_result = attrs[class_idx][1]
                self.read_tc_name = True

    def handle_data(self, data):
        if self.read_tc_name:
            self.tc_results[data] = self.tc_result
            self.read_tc_name = False

    def get_results(self):
        return self.tc_results

def get_test_results(webview, frame):
    # My version of pywebkitgtk doesn't have the WebFrame.get_dom_document() function
    # leaving no option than a hack to use JavaScript to place the html body in the
    # title so we can fetch it or getting the data via WebFrame.get_data_source;
    # which is nicer in my opinion. However, this only returns the original data not
    # the modified DOM
    doc = get_html_body(webview, frame)

    # To maximize the chance of finding the test results, search for qunit-test
    start = doc.find('<ol id="qunit-tests">')
    end = doc.rfind('</ol>', start)
    doc = doc[start:end+5] # to also include </ol>

    parser = ParseTestResults()
    parser.feed(doc)
    tc_results = parser.get_results()

    show_results(tc_results)
    determine_exit_status(tc_results)

def show_results(tc_results):
    pass_count = 0
    fail_count = 0
    for k, v in tc_results.iteritems():
        if v == 'pass':
            sys.stdout.write('.')
            pass_count += 1
        elif v == 'fail':
            sys.stdout.write('F')
            fail_count += 1
        else:
            sys.stdout.write('?')
    sys.stdout.flush()

    if verbose:
        print ""
        print ""
        for k, v in tc_results.iteritems():
            print "%s -> %s" % (k, v)

    if not verbose:
        print ""

    print ""
    print "Ran %d tests" % (pass_count + fail_count)
    print ""

def determine_exit_status(tc_results):
    global exit_status
    exit_status = 0
   
    for k, v in tc_results.iteritems():
        if v == 'fail':
            exit_status = 1

def load_finished(webview, frame):
    global global_webview
    global_webview = webview

    timer = gobject.timeout_add(500, determine_test_run_status)

def determine_test_run_status():    
    doc = get_html_body(global_webview, global_webview.get_main_frame())

    if "running" in doc or "Running" in doc:
        # Wait another round
        timer = gobject.timeout_add(500, determine_test_run_status)
    else:
        # No tests are running, check results
        get_test_results(global_webview, global_webview.get_main_frame())
        gtk.main_quit()

def form_unit_test_document(testfiles):
    tests = []
    for f in testfiles:
        tests.append('<script src=\"'+os.path.basename(f)+'\"></script>')

    dirname = os.path.abspath(os.path.dirname(__file__))
    filename = dirname + '/qunit.html'
   
    raw_html = """<html>
<head>
    <meta charset="utf-8">
    <title>test</title>
    <link rel="stylesheet" href="%s/qunit-1.15.0.css" />
</head>
<body>
    <div id="qunit"></div>
    <div id="qunit-fixture"></div>
    <script src="%s/qunit-1.15.0.js"></script>
    $OUR_UNITTESTS_HERE$
</body>
</html>""" % (dirname, dirname)

    html = raw_html.replace('$OUR_UNITTESTS_HERE$', "\n".join(tests))
    
    with open(filename+'.tmp', 'w') as f:
        f.write(html)

    return html

def main(testfiles):
    view = webkit.WebView()
    view.connect('load-finished', load_finished)
    
    settings = view.get_settings()
    settings.set_property('enable-file-access-from-file-uris', 1)
    view.set_settings(settings)

    """
    # PART MAY BE REMOVED AFTER DEBUGGING!
    sw = gtk.ScrolledWindow()
    sw.add(view)

    win = gtk.Window(gtk.WINDOW_TOPLEVEL)
    win.set_default_size(800, 600)
    win.connect('delete-event', lambda win, event: gtk.main_quit())
    win.add(sw)
    win.show_all()
    # END PART MAY BE REMOVED AFTER DEBUGGING!
    """

    test_html = form_unit_test_document(testfiles)
    #view.load_string(test_html, "text/html", "UTF-8", ".") # For some reason this just won't run the tests!
 
    view.load_uri('file://' + os.path.abspath(os.path.dirname(__file__)) + '/qunit.html.tmp')

    gtk.main()

if __name__ == '__main__':
    usage = "usage: %prog [options] [FILE]..."
    opts = OptionParser(usage)
    opts.add_option("-v", "--verbose", action="store_true", dest="verbose", help="Show which tests have ran", default=False)

    (options, args) = opts.parse_args()

    verbose = options.verbose

    if len(args) == 0:
        opts.error("No tests to be executed specified")
    
    main(args)

    # Write proper exit status so this script can be used for automation
    exit(exit_status)

